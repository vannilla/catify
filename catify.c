/* catify - substitute every word with cat. */
/* Copyright (C) 2018  Alessio Vanni */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <argp.h>
#include <errno.h>
#include <error.h>

error_t argp_parser(int key, char *arg, struct argp_state *state);

const char *argp_program_version = "1.0";
const char *argp_program_bug_address = "<vannilla@firemail.cc>";

struct argp_option argp_options[] = {
     {"word", 'w', "WORD", 0, "Use WORD instead of `cat'.", 0},
     {0}
};

struct argp argp_data = {
     argp_options, argp_parser, NULL,
     "Read words from standard input and change every word with `cat'.",
     NULL, NULL, NULL
};

char *subst = "cat";

int main(int argc, char *argv[]) {
     char *word;
     char rd;
     size_t len;
     size_t next;
     size_t r;
     
     argp_parse(&argp_data, argc, argv, 0, NULL, NULL);

     len = 512;
     next = 0;
     word = (char *)calloc(len, sizeof(char));
     if (word == NULL) {
	  error(1, errno, "Can't create buffer");
     }

     do {
     	  r = read(STDIN_FILENO, &rd, sizeof(char));

	  if (isalpha(rd)) {
	       if (len - next < len/7) {
		    len = len * 7;
		    word = (char *)realloc(word, len*sizeof(char));
		    if (word == NULL) {
			 error(1, errno, "Can't resize buffer");
		    }
	       }
	       word[next] = rd;
	       ++next;
	  } else {
	       if (next != 0) {
		    word[next] = '\0';
		    write(STDOUT_FILENO, subst, strlen(subst)*sizeof(char));
		    next = 0;
	       }
	       write(STDOUT_FILENO, &rd, sizeof(char));
	  }
     } while (r > 0);

     if (next != 0) {
	  write(STDOUT_FILENO, subst, strlen(subst)*sizeof(char));
     }

     return 0;
}

error_t argp_parser(int key, char *arg, struct argp_state *state) {
     switch (key) {
     case 'w':
	  subst = arg;
	  break;
     default:
	  return ARGP_ERR_UNKNOWN;
     }

     return 0;
}
